const path = require('path');
const fs = require('fs');
var yaml = require('js-yaml');

const readFile = (filePath) => {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf8', (err, data) => {
            if(err) {
                reject(err);
            }
            else {
                resolve(data);
            }
        } )
    });
};

const rawFile = (fileYml) => {
    return readFile(fileYml).then((ymlData)=> {
        return yaml.safeLoad(ymlData);
    }, console.error);
}

module.exports = {rawFile: rawFile};