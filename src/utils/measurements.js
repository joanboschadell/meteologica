const path = require('path');

const rawFile = require('./yml').rawFile;

const rawFileTemperatures = () => rawFile(path.join(__dirname, '../', 'assets', 'data.yml'));

const convertDKtoK = (temperatureWithdK) => {
    return temperatureWithdK ? +temperatureWithdK / 10 : null;
};

const convertKtoCelsius = (temperatureWithK) => {
    return temperatureWithK ? +temperatureWithK - 273.15 : null;
};

const convertMWtoKW = (powerWithMW) => {
    return powerWithMW ? +powerWithMW * 1000 : null;
}

const mergeTemperatureWithEnergy = () => {
    return rawFileTemperatures().then((data)=>{
        let measurements = {};
        data.temperature.values.reduce((acum, current)=> {
            acum[current.time] = acum[current.time] || {};
            let value = current.value ? current.value.toString().replace(/,/g, '.'): null;
            acum[current.time].temperature = convertKtoCelsius(convertDKtoK(value));
            return acum;
        }, measurements);

        data.power.values.reduce((acum, current)=> {
            acum[current.time] = acum[current.time] || {};
            let value = current.value ? current.value.toString().replace(/,/g, '.'): null;
            acum[current.time].power = convertMWtoKW(value);
            return acum;
        }, measurements);
        
        measurements = Object.assign({}, measurements, {
            unit: { 
                temperature: data.temperature.unit,
                power: data.power.unit 
            }
        });

        return measurements;
        

    });
};

const normalizeSecond1Digit = (intSecond) => {
    if(intSecond === 5 || intSecond === 0) {
        return intSecond;
    }
    else if(intSecond < 5) {
        return 0;
    }
    else if(intSecond < 10) {
        return 5;
    }
}

const normalizeSecond = (second) => {
    const intSecond = parseInt(second, 10);
    const lastDigitSecond = intSecond % 10;
    
    return intSecond - lastDigitSecond + normalizeSecond1Digit(lastDigitSecond)
};

const getMeasurementsFromDate = (measures, time) => {
    const hour = time.h !== undefined ? time.h.toString().padStart(2, '0') : '[0-9]{2}';
    const minute = time.m !== undefined ? time.m.toString().padStart(2, '0') : '[0-9]{2}';
    const second = time.s !== undefined ? normalizeSecond(time.s).toString().padStart(2, '0') : '[0-9]{2}';

    const pattern = [hour, minute, second].join(':');
    console.log(pattern)
    const regex = new RegExp(pattern);
    return Object.keys(measures).filter((m) => regex.test(m)).reduce((acum, time)=> {
        var measure = Object.assign({}, measures[time], {time: time});
        acum.push(measure);
        //acum[time] = measures[time];
        //return acum;
        return acum;
    }, []);
};

const sum = (measures, type) => {
    return measures.reduce((acum, current)=> acum + (+current[type]), 0);
};

const average = (measures, type) => {
    let result = 0;
    const measuresWithValue = measures
        .filter((m)=> m[type] !== undefined || m[type !== null]);
        //.map((m)=> measures[t]);
    if(measuresWithValue.length) {
        result = sum(measuresWithValue, type) / measuresWithValue.length;
    }
    return Math.round(result * 100) / 100; 
};

module.exports = {
    mergeTemperatureWithEnergy: mergeTemperatureWithEnergy,
    getMeasurementsFromDate: getMeasurementsFromDate,
    average: average,
    types: {
        temperature: "temperature",
        power: 'power'
    }
}