const express = require('express');
const app = express();
const path = require('path');

module.exports = function(rootDirectory) {
    // config
    app.set('port', process.env.PORT || 3000);

    // static files

    app.use('/', express.static(path.join(__dirname, 'public')));

    app.use('/jquery', express.static(path.join(rootDirectory, 'node_modules', 'jquery', 'dist' )));
    app.use('/moment', express.static(path.join(rootDirectory, 'node_modules', 'moment', 'min' )));

    const server = app.listen(app.get('port'), () => {
        console.log(`Server on port ${app.set('port')}`);
        
    });
    return server;
};