debugger;
const socket = io();

const $temperature = $('#chart_temperature');

const $power = $('#chart_power');
$(document).on('initSockets', function(e){
    socket.emit('currentTimeFromBrowser', new Date().getTime());
});

socket.on('lastMeasure', function(data){
    $('#currentValue').html(JSON.stringify(data, null, 4));
    $('#currentTime').html(data.time)
    $temperature.trigger('drawChart', [data.temperature]);
    $power.trigger('drawChart', [data.power]);
});

socket.on('intervalMeasure', function(data) {
    $('#intervalTemperature').trigger('drawChartInterval', data);
    $('#intervalPower').trigger('drawChartInterval', data);
});