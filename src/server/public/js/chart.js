(function() {
    var drawChart = function drawChart(params) {      
        let options = params.options;
       
        let data = google.visualization.arrayToDataTable(params.data);
        let chart = params.chart;

        let $elem = params.$elem;
        chart.draw(data, options);
       
       $elem.on('drawChart', function(e, measure) {
            console.log('drawChart:', measure);
            data.setValue(0, 1, measure);
            chart.draw(data, options);
        });
    };

    google.charts.load('current', {'packages':['gauge', 'corechart']}).then(function() {
        let optionsToTemperature = {
            width: 400, height: 120,
            redFrom: 40, redTo: 50,
            yellowFrom:30, yellowTo: 40,
            minorTicks: 5,
            max: 50
        };

        
        let dataTemperature = [
            ['Label', 'Value'],
            ['ºC', 0]
        ];

        let $temperature = $('#chart_temperature');

        let charTemperature = new google.visualization.Gauge($temperature[0]);

        drawChart({
            chart: charTemperature,
            data: dataTemperature,
            options: optionsToTemperature,
            $elem: $temperature
        });

        let optionsToPower = {
            width: 400, height: 120,
            redFrom: 100000, redTo: 120000,
            yellowFrom:80000, yellowTo: 100000,
            minorTicks: 5,
            max: 120000
        };

        let dataPower = [
            ['Label', 'Value'],
            ['kW', 0]
        ];

        let $power = $('#chart_power');

        let chartPower= new google.visualization.Gauge($power[0]);

        drawChart({
            chart: chartPower,
            data: dataPower,
            options: optionsToPower,
            $elem: $power
        });


        let optionsToTemperatureAverage = {
            title: 'Average Temperature',
            legend: { position: 'bottom' }
        };

        let dataTemperatureAverage = new google.visualization.DataTable() 
        dataTemperatureAverage.addColumn('string', 'Time');
        dataTemperatureAverage.addColumn('number', 'ºC');

        var $intervalTemperature = $('#intervalTemperature');

        var chartTemperatureAverage = new google.visualization.LineChart($intervalTemperature[0]);

        $intervalTemperature.on('drawChartInterval', function(e, measure) {
            if(measure.averageTemperature) {
                dataTemperatureAverage.addRows([[measure.time, measure.averageTemperature]]);
                chartTemperatureAverage.draw(dataTemperatureAverage, optionsToTemperatureAverage);
            }
        });



        let optionsToEnergyAverage = {
            title: 'Energy',
            legend: { position: 'bottom' }
        };

        let dataEnergyAverage = new google.visualization.DataTable() 
        dataEnergyAverage.addColumn('string', 'Time');
        dataEnergyAverage.addColumn('number', 'Kwh');

        var $intervalEnergy= $('#intervalPower');

        var chartPowerAverage = new google.visualization.LineChart($intervalEnergy[0]);

        $intervalEnergy.on('drawChartInterval', function(e, measure) {
            console.log(measure);
            if(measure.energy) {
                dataEnergyAverage.addRows([[measure.time, measure.energy]]);
                chartPowerAverage.draw(dataEnergyAverage, optionsToEnergyAverage);
            }
        });
        $(document).trigger('initSockets');
    });
})();