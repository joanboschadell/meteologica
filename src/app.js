const server = require('./server/server.js')(__dirname);

const socketIO = require('socket.io');

const moment = require('moment');

const measurements = require('./utils/measurements');

const getTime = (timeWithMoment) => {
    return {
        h: timeWithMoment.hour(),
        m: timeWithMoment.minute(), 
        s: timeWithMoment.second()
    }
};

const addSeconds = (momentTime, seconds) => {

}
// websockets
const io = socketIO(server);


io.on('connection', (socket)=> {
    const emitCurrentMeasure = (currentTime) => {
    
        measurements.mergeTemperatureWithEnergy().then((measures)=> {
            const lastMeasure = measurements.getMeasurementsFromDate(measures,
                getTime(currentTime));

            socket.emit('lastMeasure', lastMeasure[0]);
        });
    };
    console.log(`new connection id: ${socket.id}`);
    
    socket.on('currentTimeFromBrowser', function(startTime){
        let currentTime = new moment(startTime);
        
        emitCurrentMeasure(currentTime);

            setInterval(()=>{
                let passTime = new moment(currentTime);
                currentTime.add(5, 'seconds');
                emitCurrentMeasure(currentTime);
                if(passTime.minute() !== currentTime.minute()) {
                    measurements.mergeTemperatureWithEnergy().then((measures)=> {
                        let interval = measurements.getMeasurementsFromDate(measures,
                            {
                                h: passTime.hour(),
                                m: passTime.minute()
                            });
                            let averageTemperature = measurements.average(interval, measurements.types.temperature);
                            let averagePower = measurements.average(interval, measurements.types.power);
                        socket.emit('intervalMeasure', {
                            time: [passTime.hour(), passTime.minute(), '00'].join(':'),
                            averageTemperature: averageTemperature,
                            energy: averagePower / 60,
                            interval: interval
                        });
                    });
                }
            }, 5000)
    });

});
